Food_Expiry
===========
A food inventory system which sends email notifications when items expire.

Crons
=====
0 0 * * * php app/console foodexpiry:notify >> app/logs/cron.log 2>&1
