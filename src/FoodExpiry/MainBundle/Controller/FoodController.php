<?php

namespace FoodExpiry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FoodController extends Controller
{
    /**
     * Return all food items in the database
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        // get the currently logged in user
        $userId = $this->get('user_service')->getLoggedInUserId();

        $exp = $this->get('food_service')->getAllExpiringFood($userId);
        $expiringInAWeek = $exp['week'];
        $expiringToday = $exp['today'];
        $expiredLastWeek = $exp['lastweek'];
        $noExpiry = $exp['noexpiry'];

        return $this->render('FoodExpiryMainBundle:food:index.html.twig',
            array(
                'expiringInAWeek' => $expiringInAWeek,
                'expiringToday' => $expiringToday,
                'expiredLastWeek' => $expiredLastWeek,
                'noExpiry' => $noExpiry
            )
        );
    }

    public function viewAllAction()
    {
        // get the currently logged in user
        $userId = $this->get('user_service')->getLoggedInUserId();

        $allFood = $this->get('food_service')->getAllFood($userId);

        return $this->render('FoodExpiryMainBundle:food:allFood.html.twig',
            array(
                'allFood' => $allFood
            )
        );
    }

    /**
     * Add food
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $userId = $this->get('user_service')->getLoggedInUserId();
        $result = $this->get('food_service')->addFood($request, $userId);

        if ($result['success']) {
            $this->get('session')->getFlashBag()->add(
                'notice',
                'The item has been added successfully.'
            );

            return $this->redirectToRoute('home');
        }

        return $this->render('FoodExpiryMainBundle:food:manage.html.twig', array(
            'form' => $result['form']->createView(),
            'submitLabel' => 'Add'
        ));
    }

    /**
     * Edit food with $id
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $userId = $this->get('user_service')->getLoggedInUserId();
        $result = $this->get('food_service')->editFood($id, $request, $userId);

        if ($result['success']) {
            return $this->redirectToRoute('home');
        }

        return $this->render('FoodExpiryMainBundle:food:manage.html.twig', array(
            'form' => $result['form']->createView(),
            'submitLabel' => 'Save'
        ));
    }

    /**
     * Deletes food with $id
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {
        $userId = $this->get('user_service')->getLoggedInUserId();
        $result = $this->get('food_service')->deleteFood($id, $userId);

        $this->get('session')->getFlashBag()->add(
            'notice',
            'The item has been deleted successfully.'
        );

        return $this->redirectToRoute('home');
    }
}