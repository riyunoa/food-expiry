<?php

namespace FoodExpiry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FoodType extends AbstractType
{
    protected $formName = 'foodForm';
    /**
     * Creates the form
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('expiry')
            ->add('quantity')
            ->add('submit', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FoodExpiry\MainBundle\Entity\Food'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->formName;
    }
}