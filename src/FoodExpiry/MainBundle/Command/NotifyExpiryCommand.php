<?php

namespace FoodExpiry\MainBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class NotifyExpiryCommand extends ContainerAwareCommand
{
    /**
     * Set the command parameters
     */
    protected function configure()
    {
        $this
            ->setName('foodexpiry:notify')
            ->setDescription('Notify of expiring items');
    }

    /**
     * Sends email to user with list of expiring food
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Food expiry notification run at ' . date('Y-m-d H:i:s'));
        $result = $this->getContainer()->get('food_service')->notifyFoodExpiringInPeriod();
    }
}