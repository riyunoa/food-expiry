<?php

namespace FoodExpiry\MainBundle\Service;

use FoodExpiry\MainBundle\Entity\Food;
use Symfony\Component\HttpFoundation\Request;
use FoodExpiry\MainBundle\Form\FoodType;

class FoodService
{
    protected $db;
    protected $formFactory;
    protected $templating;
    protected $mailer;

    public function __construct($db, $formFactory, $templating, $mailer)
    {
        $this->db = $db;
        $this->formFactory = $formFactory;
        $this->templating = $templating;
        $this->mailer = $mailer;
    }

    /**
     * Returns all food added to the database
     * @return mixed
     */
    public function getAllFood($userId = 0)
    {
        $foodArray = array();

        if (!$userId) {
            $foodArray = $this->db
                ->getRepository('FoodExpiryMainBundle:Food')
                ->findAll();
        }
        else {
            $queryBuilder = $this->db->getRepository('FoodExpiryMainBundle:Food')->createQueryBuilder('f');
            $queryBuilder
                ->where('f.userId = :userId')
                ->setParameter('userId', (int)$userId);
            $queryBuilder->orderBy('f.expiry', 'ASC');
            $query = $queryBuilder->getQuery();

            $foodArray = $query->getResult();
        }

        return $foodArray;
    }

    /**
     * Add a food item
     * @param Request $request
     * @return mixed
     */
    public function addFood(Request $request, $userId = 0)
    {
        if (!$userId) {
            return array('success' => false);
        }

        $food = new Food();
        $food->setUserId($userId);
        $form = $this->formFactory->create(new FoodType(), $food);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->saveFood($food);
            return array('success' => true);
        }

        return array(
            'success' => false,
            'form' => $form);
    }

    /**
     * Edit a food item with $id
     * @param $id
     * @param Request $request
     * @return array
     */
    public function editFood($id, Request $request, $userId = 0)
    {
        if (!$userId) {
            return array('success' => false);
        }

        $food = $this->db
            ->getRepository('FoodExpiryMainBundle:Food')
            ->find((int)$id);

        if ($food->getUserId() != $userId) {
            return array('success' => false);
        }

        if (empty($food)) {
            // redirect to home page
            return array('success' => true);
        }

        $form = $this->formFactory->create(new FoodType(), $food);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->saveFood($food);
            return array('success' => true);
        }

        return array(
            'success' => false,
            'form' => $form);
    }

    /**
     * Delete food with $id
     * @param $id
     * @return bool
     */
    public function deleteFood($id, $userId = 0)
    {
        if (!$userId) {
            return false;
        }

        $food = $this->db
            ->getRepository('FoodExpiryMainBundle:Food')
            ->find((int)$id);

        if ($food->getUserId() != $userId) {
            return false;
        }

        if (!empty($food)) {
            $entityManager = $this->db->getManager();
            $entityManager->remove($food);
            $entityManager->flush();

            return true;
        }

        return false;
    }

    /**
     * Save food entity to database
     * @param $food
     */
    public function saveFood($food)
    {
        $entityManager = $this->db->getManager();
        $entityManager->persist($food);
        $entityManager->flush();
    }

    /**
     * Returns food expiring in period bounded by $startDate and $endDate
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    public function getFoodExpiringInPeriod($startDate = 0, $endDate = 0, $userId = 0)
    {
        $queryBuilder = $this->db->getRepository('FoodExpiryMainBundle:Food')->createQueryBuilder('f');

        if ($userId) {
            $queryBuilder
                ->where('f.userId = :userId')
                ->setParameter('userId', $userId);
        }

        if ($startDate && $endDate) {
            $queryBuilder->andWhere('f.expiry BETWEEN :startDate AND :endDate');
        } elseif ($startDate) {
            $queryBuilder->andWhere('f.expiry > :startDate');
        } elseif ($endDate) {
            $queryBuilder->andWhere('f.expiry < :endDate');
        }

        if ($startDate) {
            $queryBuilder->setParameter('startDate', $startDate->format('Y-m-d'));
        }

        if ($endDate) {
            $queryBuilder->setParameter('endDate', $endDate->format('Y-m-d'));
        }

        $queryBuilder->orderBy('f.expiry', 'ASC');
        $query = $queryBuilder->getQuery();

        $food = $query->getResult();

        return $food;
    }

    /**
     * Sends email to all users with expiring items
     * @return bool
     */
    public function notifyFoodExpiringInPeriod()
    {
        //get all expiring food for all users
        $exp = $this->getAllExpiringFood();
        $expiringToday = $exp['today'];
        $expiringInAWeek = $exp['week'];
        $expiredLastWeek = $exp['lastweek'];
        $noExpiry = $exp['noexpiry'];

        if (empty($expiringInAWeek) && empty($expiringToday) && empty($expiredLastWeek) && empty($noExpiry)) {
            return false;
        }

        $keyGetterFunc = 'getUserId';
        // group the results by user id
        $expiringInAWeek = $this->groupByKey($expiringInAWeek, $keyGetterFunc);
        $expiringToday = $this->groupByKey($expiringToday, $keyGetterFunc);
        $expiredLastWeek = $this->groupByKey($expiredLastWeek, $keyGetterFunc);
        $noExpiry = $this->groupByKey($noExpiry, $keyGetterFunc);

        // get all the users who need to be notified
        $userIdList = array_keys($expiringInAWeek + $expiringToday + $expiredLastWeek + $noExpiry);

        foreach($userIdList as $userId) {
            $templateData = array(
                'expiringInAWeek' => !empty($expiringInAWeek[$userId]) ? $expiringInAWeek[$userId] : array(),
                'expiringToday' => !empty($expiringToday[$userId]) ? $expiringToday[$userId] : array(),
                'expiredLastWeek' => !empty($expiredLastWeek[$userId]) ? $expiredLastWeek[$userId] : array(),
                'noExpiry' => !empty($noExpiry[$userId]) ? $noExpiry[$userId] : array(),
            );

            $this->sendNotifications($userId, $templateData);
        }
        return true;
    }

    /**
     * Send notifications to the user with $userId as well as the extra addresses attached to that account
     * @param $userId
     * @param $templateData
     * @return bool
     */
    public function sendNotifications($userId, $templateData) {
        // get the user
        $user = $this->db
            ->getRepository('FoodExpiryMainBundle:User')
            ->find($userId);

        if (empty($user)) {
            echo "User $userId does not exist \n";
            return false;
        }

        $toUsers = array();
        $toUsers[$user->getEmail()] = $user->getFirstName() . ' ' . $user->getLastName();

        // get the other email addresses to send to
        $extraNotifications = $this->db
            ->getRepository('FoodExpiryMainBundle:ExtraNotification')
            ->findBy(array('userId' => $userId));

        foreach ($extraNotifications as $extraUser) {
            $toUsers[$extraUser->getEmail()] = $extraUser->getName();
        }

        echo "Sending to: " . implode(',', array_keys($toUsers)) . "\n";
        // send email to the users
        $message = $this->mailer->createMessage()
            ->setSubject('Expiring food notification')
            ->setFrom(array('info@myfoodexpiry.com' => 'Food Expiry Notifier'))
            ->setTo($toUsers)
            ->setBody(
                $this->templating->render(
                    'FoodExpiryMainBundle:emails:expiring.html.twig',
                    $templateData
                ),
                'text/html'
            );

        $this->mailer->send($message);
        return true;
    }

    /**
     * Group an array of objects by key
     * @param array $expiringItems
     * @param $key
     * @return array
     */
    public function groupByKey(array $expiringItems, $keyGetterFunc)
    {
        $output = array();
        foreach ($expiringItems as $i) {
            $output[$i->$keyGetterFunc()][] = $i;
        }

        return $output;
    }

    /**
     * Gets all expiring food
     */
    public function getAllExpiringFood($userId = 0)
    {
        $expiringInAWeek = $this->getFoodExpiringInPeriod(new \DateTime('+1 day'), new \DateTime('+1 week'), $userId);
        $expiringToday = $this->getFoodExpiringInPeriod(new \DateTime(), new \DateTime(), $userId);
        $expiredLastWeek = $this->getFoodExpiringInPeriod(new \DateTime('-1 week'), new \DateTime('-1 day'), $userId);
        $noExpiryDate = $this->getFoodNoExpiryDate();

        return array(
            'today' => $expiringToday,
            'week' => $expiringInAWeek,
            'lastweek' => $expiredLastWeek,
            'noexpiry' => $noExpiryDate
        );
    }

    /**
     * Get food with no expiry date set
     * @param int $userId
     * @return mixed
     */
    public function getFoodNoExpiryDate($userId = 0)
    {
        $findBy = array();

        if ($userId) {
            $findBy['userId'] = $userId;
        }

        $findBy['expiry'] = null;

        $foodWithNoExpiryDate = $this->db
            ->getRepository('FoodExpiryMainBundle:Food')
            ->findBy($findBy);

        return $foodWithNoExpiryDate;
    }
}