<?php

namespace FoodExpiry\MainBundle\Service;

use Symfony\Component\HttpFoundation\Request;

class UserService
{
    protected $db;
    protected $securityTokenStorage;

    public function __construct($db, $securityTokenStorage)
    {
        $this->db = $db;
        $this->securityTokenStorage = $securityTokenStorage;
    }

    /**
     * Get the user id of the logged in user
     * @return int
     */
    public function getLoggedInUserId()
    {
        if (null === $token = $this->securityTokenStorage->getToken()) {
            return 0;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return 0;
        }

        return $user->getId();
    }
}